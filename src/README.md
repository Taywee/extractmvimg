# extractmvimg

Simply extracts the mp4 from mvimg files as recorded by Google Pixel phones.
This currently calls out to exiftool, which needs to reside in your PATH, but I
hope to change that at some point in the future.
