use std::collections::HashMap;
use std::convert::AsRef;
use std::convert::TryInto;
use std::env;
use std::ffi::OsStr;
use std::fs::File;
use std::io::SeekFrom;
use std::io::Seek;
use std::io::Read;
use std::io;
use std::path::Path;
use std::process::{Command, Stdio};
use std::str;

/**
 * Simple convenience trait for retrieving some exif fields and the offset to the mvimg embedded
 * video.
 */
trait ExifPath {
    fn get_fields(&self) -> io::Result<HashMap<String, String>>;
    fn get_offset(&self) -> io::Result<i64>;
}

impl<T: AsRef<Path>> ExifPath for T {
    fn get_fields(&self) -> io::Result<HashMap<String, String>> {
        let output = Command::new("exiftool")
            .args(&[OsStr::new("-xmp:all"), self.as_ref().as_os_str()])
            .stdin(Stdio::null())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?
            .wait_with_output()?;

        if !output.status.success() {
            return Err(io::Error::new(io::ErrorKind::Other, format!("exiftool exited with status {}.  Stderr: {}", output.status, String::from_utf8_lossy(&output.stderr))));
        }

        Ok(str::from_utf8(&output.stdout).unwrap().lines().filter_map(|line| {
            let fields: Vec<&str> = line.split(':').map(str::trim).collect();
            if fields.len() == 2 {
                Some((String::from(fields[0]), String::from(fields[1])))
            } else {
                None
            }
        }).collect())
    }

    fn get_offset(&self) -> io::Result<i64> {
        let fields = self.get_fields()?;
        let raw_value = fields.get("Micro Video Offset").ok_or(
            io::Error::new(io::ErrorKind::Other, format!("Couldn't find 'Micro Video Offset' for path {}", self.as_ref().display()))
        )?;
        Ok(raw_value.parse().map_err(|e| 
                io::Error::new(io::ErrorKind::Other, format!("Found 'Micro Video Offset' for path {}, but couldn't parse its value {:?} to a number: {}", self.as_ref().display(), raw_value, e))
        )?)
    }
}

fn main() -> io::Result<()> {
    for path in env::args().skip(1) {
        let path: &dyn AsRef<Path> = &path;
        let offset = path.get_offset()?;
        let mut input = File::open(&path)?;
        // Set offset from relative to global offset
        let offset = input.seek(SeekFrom::End(-offset))?;
        let outpath = path.as_ref().with_extension("mp4");
        let mut output = File::create(&outpath)?;
        io::copy(&mut input, &mut output)?;
        println!("Wrote file {}", outpath.display());
        input.seek(SeekFrom::Start(0))?;
        let outpath = path.as_ref().with_extension("new-jpg");
        let mut output = File::create(&outpath)?;
        io::copy(&mut input.take(offset.try_into().unwrap()), &mut output)?;
        println!("Wrote file {}", outpath.display());
    }
    Ok(())
}
